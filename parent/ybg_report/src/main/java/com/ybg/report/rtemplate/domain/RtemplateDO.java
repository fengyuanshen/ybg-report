package com.ybg.report.rtemplate.domain;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/** 报表EXCEL模板 @author Deament
 * 
 * @email 591518884@qq.com
 * @date 2018-02-21 */
@ApiModel("报表EXCEL模板")
public class RtemplateDO implements Serializable {
	
	private static final long	serialVersionUID	= 1L;
	/****/
	@ApiModelProperty(name = "id", dataType = "java.lang.String", value = "", hidden = false)
	private String				id;
	/** 文件名 **/
	@ApiModelProperty(name = "filename", dataType = "java.lang.String", value = "文件名", hidden = false)
	private String				filename;
	/****/
	@ApiModelProperty(name = "gmtCreate", dataType = "java.lang.String", value = "", hidden = false)
	private String				gmtCreate;
	/****/
	@ApiModelProperty(name = "gmtModified", dataType = "java.lang.String", value = "", hidden = false)
	private String				gmtModified;
	/** 模板内容 **/
	@ApiModelProperty(name = "filecontent", dataType = "java.lang.String", value = "模板内容", hidden = false)
	private String				filecontent;
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getId() {
		return id;
	}
	
	public void setFilename(String filename) {
		this.filename = filename;
	}
	
	public String getFilename() {
		return filename;
	}
	
	public void setGmtCreate(String gmtCreate) {
		this.gmtCreate = gmtCreate;
	}
	
	public String getGmtCreate() {
		return gmtCreate;
	}
	
	public void setGmtModified(String gmtModified) {
		this.gmtModified = gmtModified;
	}
	
	public String getGmtModified() {
		return gmtModified;
	}
	
	public void setFilecontent(String filecontent) {
		this.filecontent = filecontent;
	}
	
	public String getFilecontent() {
		return filecontent;
	}
}
