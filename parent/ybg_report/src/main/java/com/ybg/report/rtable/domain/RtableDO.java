package com.ybg.report.rtable.domain;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/** @author Deament
 * @email 591518884@qq.com
 * @date 2018-02-22 */
@ApiModel("实体（数据库)")
public class RtableDO implements Serializable {
	
	private static final long	serialVersionUID	= 1L;
	/****/
	@ApiModelProperty(name = "id", dataType = "java.lang.String", value = "", hidden = false)
	private String				id;
	/** 报表编码 **/
	@ApiModelProperty(name = "reportno", dataType = "java.lang.String", value = "", hidden = false)
	private String				reportno;
	/** 报表名称 **/
	@ApiModelProperty(name = "reportname", dataType = "java.lang.String", value = "", hidden = false)
	private String				reportname;
	/** 报表年度 **/
	@ApiModelProperty(name = "reportyear", dataType = "java.lang.Integer", value = "", hidden = false)
	private Integer				reportyear;
	/****/
	@ApiModelProperty(name = "gmtCreate", dataType = "java.lang.String", value = "", hidden = false)
	private String				gmtCreate;
	/****/
	@ApiModelProperty(name = "gmtModified", dataType = "java.lang.String", value = "", hidden = false)
	private String				gmtModified;
	/** 数据区域 **/
	@ApiModelProperty(name = "dataarea", dataType = "java.lang.String", value = "数据区域", hidden = false)
	private String				dataarea;
	/** EXCEL模版ID **/
	@ApiModelProperty(name = "tempid", dataType = "java.lang.String", value = "EXCEL模版ID", hidden = false)
	private String				tempid;
	@ApiModelProperty(name = "storetablename", dataType = "java.lang.String", value = "存储的表名", hidden = false)
	private String				storetablename;
	
	public String getStoretablename() {
		return storetablename;
	}
	
	public void setStoretablename(String storetablename) {
		this.storetablename = storetablename;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getId() {
		return id;
	}
	
	public void setReportno(String reportno) {
		this.reportno = reportno;
	}
	
	public String getReportno() {
		return reportno;
	}
	
	public void setReportname(String reportname) {
		this.reportname = reportname;
	}
	
	public String getReportname() {
		return reportname;
	}
	
	public void setReportyear(Integer reportyear) {
		this.reportyear = reportyear;
	}
	
	public Integer getReportyear() {
		return reportyear;
	}
	
	public void setGmtCreate(String gmtCreate) {
		this.gmtCreate = gmtCreate;
	}
	
	public String getGmtCreate() {
		return gmtCreate;
	}
	
	public void setGmtModified(String gmtModified) {
		this.gmtModified = gmtModified;
	}
	
	public String getGmtModified() {
		return gmtModified;
	}
	
	public void setDataarea(String dataarea) {
		this.dataarea = dataarea;
	}
	
	public String getDataarea() {
		return dataarea;
	}
	
	public void setTempid(String tempid) {
		this.tempid = tempid;
	}
	
	public String getTempid() {
		return tempid;
	}
}
