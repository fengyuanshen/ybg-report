package com.ybg.report.departmentable.dao;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Repository;
import com.ybg.base.jdbc.BaseDao;
import com.ybg.base.jdbc.BaseMap;
import com.ybg.base.jdbc.util.QvoConditionUtil;
import com.ybg.base.util.Page;
import com.ybg.report.departmentable.domain.DepartmenTableVO;
import com.ybg.report.departmentable.qvo.DepartmenTableQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import com.ybg.base.jdbc.DataBaseConstant;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
/**
 * 
 * 
 * @author Deament
 * @email 591518884@qq.com
 * @date 2018-02-21
 */
@Repository
public class DepartmenTableDaoImpl extends BaseDao implements DepartmenTableDao {
  
	@Autowired
  /**注入你需要的数据库**/
	@Qualifier(DataBaseConstant.JD_REPORT)
	JdbcTemplate jdbcTemplate;
  
	@Override
	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	private static String  QUERY_TABLE_COLUMN= "  departmenTable.reportid, departmenTable.departmentid, departmenTable.id";
	private static String  QUERY_TABLE_NAME="report_departmen_table  departmenTable";
	@Override
	public DepartmenTableVO save(DepartmenTableVO departmenTable) throws Exception {
		BaseMap<String, Object> createmap = new BaseMap<String, Object>();
		String id = null;		
		 createmap.put("reportid", departmenTable.getReportid());
		 createmap.put("departmentid", departmenTable.getDepartmentid());
		 		id = baseCreate(createmap, "report_departmen_table", "id");
		departmenTable.setId((String) id);
		return departmenTable;
	}
	@Override
	public void update(BaseMap<String, Object> updatemap,
			BaseMap<String, Object> WHEREmap) {
		this.baseupdate(updatemap, WHEREmap, "report_departmen_table");
	}
	@Override
	public Page list(Page page,  DepartmenTableQuery qvo)throws Exception {
		StringBuilder sql = new StringBuilder();
		sql.append(SELECT).append(QUERY_TABLE_COLUMN).append(FROM)
				.append(QUERY_TABLE_NAME);
		sql.append(getcondition(qvo));
		page.setTotals(queryForInt(sql));
		if (page.getTotals() > 0) {
			page.setResult(getJdbcTemplate().query(page.getPagesql(sql),
					 new BeanPropertyRowMapper<DepartmenTableVO>(DepartmenTableVO.class)));
		} else {
			page.setResult(new ArrayList< DepartmenTableVO>());
		}

		return page;
	}

	private String getcondition( DepartmenTableQuery qvo) throws Exception{
		StringBuilder sql = new StringBuilder();
		sql.append(WHERE).append("1=1");		
      sqlappen(sql, "departmenTable.id", qvo.getId());sqlappen(sql, "departmenTable.reportid", qvo.getReportid());sqlappen(sql, "departmenTable.departmentid", qvo.getDepartmentid());		return sql.toString();
	}
	@Override
	public List<DepartmenTableVO> list(DepartmenTableQuery qvo) throws Exception{
		StringBuilder sql = new StringBuilder();
		sql.append(SELECT).append(QUERY_TABLE_COLUMN).append(FROM).append(QUERY_TABLE_NAME);
		sql.append(getcondition(qvo));
		return getJdbcTemplate().query(sql.toString(), new BeanPropertyRowMapper<DepartmenTableVO>(DepartmenTableVO.class));
	}
	
	@Override
	public void remove(BaseMap<String, Object> wheremap) {
		 baseremove(wheremap, "report_departmen_table");		
	}
	@Override
	public DepartmenTableVO get(String id){	
		StringBuilder sql = new StringBuilder();
		sql.append(SELECT).append(QUERY_TABLE_COLUMN).append(FROM).append(QUERY_TABLE_NAME);
		sql.append(WHERE).append("1=1");
		sql.append(AND).append("id='"+id+"'");
		List<DepartmenTableVO> list=getJdbcTemplate().query(sql.toString(), new BeanPropertyRowMapper<DepartmenTableVO>(DepartmenTableVO.class));		
		return QvoConditionUtil.checkList(list)?list.get(0):null;
	}
}
